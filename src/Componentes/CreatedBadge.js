import React, { Component } from 'react'
const qrcode = require("qrcode-generator");
var qr;

export default class CreatedBadge extends Component {
    // This class is for display purposes, deletion and editing will be done in Home.js
    constructor() {
        super();

        this.state = {
            qrcodeTag: ""
        }
    }

    componentWillMount() {
        var typeNumber = 10;
        var errorCorrectionLevel = 'L';
        qr = qrcode(typeNumber, errorCorrectionLevel);

        var data = "Name: " + this.props.data["name"] +
            "\nEmail: " + this.props.data["email"] +
            "\nTwitter: " + this.props.data["twitter"] +
            "\nGithub: " + this.props.data["github"];

        qr.addData(data);
        qr.make();

        this.setState({
            qrcodeTag: qr.createImgTag()
        });

    }

    componentWillReceiveProps() {
        var typeNumber = 10;
        var errorCorrectionLevel = 'L';
        qr = qrcode(typeNumber, errorCorrectionLevel);

        var data = "Name: " + this.props.data["name"] +
            "\nEmail: " + this.props.data["email"] +
            "\nTwitter: " + this.props.data["twitter"] +
            "\nGithub: " + this.props.data["github"];

        qr.addData(data);
        qr.make();

        this.setState({
            qrcodeTag: qr.createImgTag()
        });
    }

    render() {
        return (
            <div className="card">
                <div className="card-header">
                    <h4>{this.props.data["name"]}</h4>
                </div>
                <div className="card-body">
                    <div className="row">
                        <div className="col-6">
                            <b>Email: </b>{this.props.data["email"]}
                            {this.props.data["twitter"] ? <div><b>Twitter: @</b>{this.props.data["twitter"]}</div> : ""}
                            {this.props.data["github"] ? <div><b>Github: </b>{this.props.data["github"]}</div> : ""}
                        </div>
                        <div className="col-6">
                            <div className="full-width" dangerouslySetInnerHTML={{
                                __html: this.state.qrcodeTag
                            }}>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-6">
                            <button className="btn btn-primary full-width" onClick={() => this.props.printBadge(this.props.data_key)}>Print</button>
                        </div>
                        <div className="col-6">
                            <button className="btn btn-primary full-width" onClick={() => this.props.editBadge(this.props.data_key)}>Edit</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
