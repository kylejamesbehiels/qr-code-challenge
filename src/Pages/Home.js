import React, { Component } from 'react'
import { FaPlus, FaTimes } from 'react-icons/fa';
import CreatedBadge from '../Componentes/CreatedBadge';
import { renderToString} from 'react-dom/server';

export default class Home extends Component {

    constructor(){
        super();
        this.state = {
            creating_badge: false,
            badges: [],
            editing: false,
            editing_index: -1
        }

        this.toggleNewBadge = this.toggleNewBadge.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handleTwitterChange = this.handleTwitterChange.bind(this);
        this.handleGithubChange = this.handleGithubChange.bind(this);
        this.createNewBadge = this.createNewBadge.bind(this);
        this.editBadge = this.editBadge.bind(this);
        this.printBadge = this.printBadge.bind(this);
        this.renderBadges = this.renderBadges.bind(this);
    }

    toggleNewBadge(){
        this.setState({
            creating_badge: !this.state.creating_badge,
            name: "",
            email: "",
            twitter: "",
            github: "",
            editing: false,
            editing_index: -1
        })
    }

    renderBadges(){
        this.setState({
            rendered_badges: ""
        }, () => {
            var rendered_badges = [];
            var index = 0;
            this.state.badges.forEach((badge) => {
                rendered_badges.push(<CreatedBadge id={"badge-" + index} key={index} data_key={index} data={badge} editBadge={this.editBadge} printBadge={this.printBadge}></CreatedBadge>);
                index += 1;
            })
    
            this.setState({
                rendered_badges: rendered_badges
            })
        })
    }

    handleNameChange(e){
      this.setState({
          name: e.target.value
      });
    }
    handleEmailChange(e){
        this.setState({
            email: e.target.value
        });
      }
      handleTwitterChange(e){
        this.setState({
            twitter: e.target.value
        });
      }
      handleGithubChange(e){
        this.setState({
            github: e.target.value
        });
      }

      editBadge(index){
        var badge = this.state.badges[index];
        this.setState({
            editing: true,
            editing_index: index,
            name: badge["name"],
            email: badge["email"],
            twitter: badge["twitter"],
            github: badge["github"],
            creating_badge: true
        }, () => {
            
        });
      }

      printBadge(index){
        var printContents = this.state.rendered_badges[index];
        var newWin = window.frames["printf"];
        newWin.document.write(renderToString(printContents));
        newWin.print();
        newWin.document.close();
      }

      createNewBadge(){
        var allVerified = true;
        var forms = document.getElementsByClassName("verify-me");
        for(var i = 0; i < forms.length; i++){
            if(!forms.item(i).checkValidity()){
                forms.item(i).classList.add("is-invalid");
                allVerified = false;
            }
            else{
                forms.item(i).classList.remove("is-invalid");
            }
        }
        if(!allVerified){
            return;
        }
        // Fields are verified, create new QR code
        var badges = this.state.badges;
        if(!this.state.editing){
            badges.push({
                name: this.state.name,
                email: this.state.email,
                twitter: this.state.twitter,
                github: this.state.github
            })
        }
        else{
            badges[this.state.editing_index] = {
                name: this.state.name,
                email: this.state.email,
                twitter: this.state.twitter,
                github: this.state.github
            }
        }
        this.setState({
            name: "",
            email: "",
            twitter: "",
            github: "",
            badges: badges,
            creating_badge: false,
            editing: false,
            editing_index: -1
        }, () => {
            this.renderBadges();
        });
      }

    render() {
        return (
            <div>
                <div className="jumbotron">
                    <div className="container">
                        <h2>QR Code Generator <hr></hr><small>Create a VR badge for yourself! Get started below.</small></h2>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <button className={"btn full-width btn-" + (this.state.creating_badge ? "danger" : "success")} onClick={this.toggleNewBadge}>{this.state.creating_badge ? <div>Cancel <FaTimes></FaTimes></div> : <div>New Badge <FaPlus></FaPlus></div>}</button>
                        </div>
                    </div>
                    <div className={(this.state.creating_badge ? "" : "hidden ") + "card"}>
                        <div className="card-header">
                            <h2>New Badge</h2>
                        </div>
                        <div className="car-body">
                            <div className="row code-form">
                                <div className="col-sm-12 col-md-6">
                                    <input className="form-control verify-me" value={this.state.name} onChange={this.handleNameChange} type="text" placeholder="Name" required></input>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <input className="form-control verify-me" value={this.state.email} onChange={this.handleEmailChange} type="email" placeholder="Email" required></input>
                                </div>
                            </div>
                            <div className="row code-form">
                                <div className="col-sm-12 col-md-6 input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">@</span>
                                    </div>
                                    <input className="form-control" value={this.state.twitter} onChange={this.handleTwitterChange} type="text" placeholder="Twitter Handle"></input>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <input className="form-control" value={this.state.github} onChange={this.handleGithubChange} type="text" placeholder="Github Account Name"></input>
                                </div>
                            </div>
                            <div className="row code-form">
                                <div className="col-sm-12 col-md-6">
                                    <button className="btn btn-success full-width" onClick={this.createNewBadge}>Save</button>
                                </div>
                                <div className="col-sm-12 col-md-6">
                                    <button className="btn btn-danger full-width" onClick={this.toggleNewBadge}>Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr></hr>
                    <div className="">
                        <h2>Badges<br></br><small>Created badges will be displayed here</small></h2>
                         {this.state.rendered_badges}
                    </div>
                </div>
                <iframe className="hidden" id="printf" name="printf"></iframe>
            </div>
        )
    }
}
